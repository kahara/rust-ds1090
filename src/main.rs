///! dump1090 to ZeroMQ bridge
use clap::{crate_authors, crate_version, App, Arg};
use datastreamcorelib::logging::init_logging_from_verbosity;
use datastreamservicelib::utils::{parse_config_file, setup_signals_termination};
use ds1090::defaultconfig::default_config_str;
use ds1090::mainloop;
use failure::Fallible;
use std::path::Path;
use tokio::runtime;

/// dump1090 to ZeroMQ bridge
fn main() -> Fallible<()> {
    let app = App::new("ds1090")
        .about("dump1090 to ZeroMQ bridge")
        .version(crate_version!())
        .author(crate_authors!())
        .arg(
            Arg::with_name("verbose")
                .short("v")
                .long("verbose")
                .multiple(true)
                .help("Increases logging verbosity each use for up to 3 times"),
        )
        .arg(
            Arg::with_name("defaultconfig")
                .long("defaultconfig")
                .help("Dump default config to stdout"),
        )
        .arg(
            Arg::with_name("configfile")
                .takes_value(true)
                .index(1)
                .empty_values(false)
                .required_unless("defaultconfig")
                .help("Config file path"),
        );
    let app_args = app.get_matches();
    if app_args.is_present("defaultconfig") {
        println!("{}", default_config_str());
        return Ok(());
    }

    let verbosity: u64 = app_args.occurrences_of("verbose");
    init_logging_from_verbosity(verbosity)?;

    let configpath = Path::new(app_args.value_of("configfile").unwrap());
    let parsed_config = parse_config_file(configpath)?;

    // Use the non-threaded scheduler explicitly
    let term = setup_signals_termination()?;
    let mut rt = runtime::Builder::new()
        .basic_scheduler()
        .enable_time()
        .enable_io()
        .build()?;
    rt.block_on(mainloop(parsed_config, term))?;
    Ok(())
}
