/// Return default configuration as string
pub fn default_config_str() -> String {
    return String::from(
        r###"
[zmq]
pub_sockets = ["ipc:///tmp/ds1090_pub.sock", "tcp://*:62624"]

[process]
track = true
required_fields = ["received", "callsign", "altitude", "ground_speed", "heading", "latitude", "longitude"]
turnaround = 900

[dump1090]
host = "127.0.0.1"
port = 30003
"###,
    )
    .trim()
    .to_string();
}
