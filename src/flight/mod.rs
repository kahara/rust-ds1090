use chrono::{DateTime, Duration, SecondsFormat, Utc};
use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::str::FromStr;
use uuid::Uuid;

#[derive(Default, Clone, Debug)]
pub struct Message {
    valid: bool,
    msgtype: Option<i8>,
    address: Option<u32>,
    received: Option<String>,
    systemtime: Option<String>,
    callsign: Option<String>,
    altitude: Option<i32>,
    ground_speed: Option<i32>,
    heading: Option<u16>,
    latitude: Option<f32>,
    longitude: Option<f32>,
    vertical_rate: Option<i32>,
    squawk: Option<u16>,
    squawk_change: Option<i8>,
    squawk_alert: Option<i8>,
    squawk_ident: Option<i8>,
    on_ground: Option<i8>,
}

impl Message {
    pub fn valid(self: &Self) -> bool {
        self.valid
    }

    pub fn uniq(self: &Self) -> u64 {
        match self.address {
            Some(address) => address as u64,
            None => 0,
        }
    }

    // FIXME: This should happen automagically with Serde
    pub fn json(self: &Self) -> serde_json::Value {
        serde_json::json!({
            "valid": self.valid,
            "msgtype": self.msgtype,
            "address": self.address,
            "received": self.received,
            "systemtime": self.systemtime,
            "callsign": self.callsign,
            "altitude": self.altitude,
            "ground_speed": self.ground_speed,
            "heading": self.heading,
            "latitude": self.latitude,
            "longitude": self.longitude,
            "vertical_rate": self.vertical_rate,
            "squawk": self.squawk,
            "squawk_change": self.squawk_change,
            "squawk_alert": self.squawk_alert,
            "squawk_ident": self.squawk_ident,
            "on_ground": self.on_ground,
        })
    }

    pub fn parse(line: &str) -> Message {
        const EXPECTED_FIELD_COUNT: usize = 22;
        let mut fields: Vec<String> = Vec::new();
        for field in line.split(",") {
            fields.push(field.to_string());
        }
        let mut message: Message = Default::default();

        if fields.len() != EXPECTED_FIELD_COUNT
            || !fields[0].eq("MSG")
            || fields[4].is_empty()
            || fields[6].is_empty()
            || fields[7].is_empty()
        {
            message.valid = false;
            return message;
        } else {
            message.valid = true;
        }

        message.msgtype = match fields[1].parse::<i8>() {
            Ok(value) => Some(value),
            Err(_) => None,
        };
        message.address = match u32::from_str_radix(&fields[4], 16) {
            Ok(address) => Some(address),
            Err(_) => None,
        };
        message.received = match DateTime::parse_from_rfc3339(&format!(
            "{}T{}Z",
            &fields[6].replace("/", "-"),
            &fields[7]
        )) {
            Ok(ts) => Some(ts.to_rfc3339_opts(SecondsFormat::Millis, true)),
            Err(_) => None,
        };
        message.systemtime = Some(Utc::now().to_rfc3339_opts(SecondsFormat::Millis, true));
        if fields[10].len() > 0 {
            message.callsign = Some(fields[10].to_string().trim().to_string());
        } else {
            message.callsign = None
        }
        message.altitude = match i32::from_str(&fields[11]) {
            Ok(value) => Some(value),
            Err(_) => None,
        };
        message.ground_speed = match i32::from_str(&fields[12]) {
            Ok(value) => Some(value),
            Err(_) => None,
        };
        message.heading = match u16::from_str(&fields[13]) {
            Ok(value) => Some(value),
            Err(_) => None,
        };
        message.vertical_rate = match i32::from_str(&fields[16]) {
            Ok(value) => Some(value),
            Err(_) => None,
        };
        message.latitude = match f32::from_str(&fields[14]) {
            Ok(value) => Some(value),
            Err(_) => None,
        };
        message.longitude = match f32::from_str(&fields[15]) {
            Ok(value) => Some(value),
            Err(_) => None,
        };
        message.squawk = match u16::from_str_radix(&fields[17], 8) {
            Ok(squawk) => Some(squawk),
            Err(_) => None,
        };
        message.squawk_change = match i8::from_str(&fields[18]) {
            Ok(value) => Some(value),
            Err(_) => None,
        };
        message.squawk_alert = match i8::from_str(&fields[19]) {
            Ok(value) => Some(value),
            Err(_) => None,
        };
        message.squawk_ident = match i8::from_str(&fields[20]) {
            Ok(value) => Some(value),
            Err(_) => None,
        };
        message.on_ground = match i8::from_str(&fields[21]) {
            Ok(value) => Some(value),
            Err(_) => None,
        };

        Message { ..message }
    }
}

#[derive(Debug, Clone)]
pub struct Flight {
    pub uuid: Uuid,
    pub values: Message,
}

impl Flight {
    pub fn new() -> Self {
        let uuid = Uuid::new_v4();
        Flight {
            uuid: uuid,
            values: Message::default(),
        }
    }

    pub fn reset(self: &mut Self) {
        self.uuid = Uuid::new_v4();
        self.values = Message::default();
    }

    pub fn update(
        self: &mut Self,
        message: &mut Message,
        required_fields: &Vec<String>,
    ) -> Option<&Self> {
        let mut required_count: usize = required_fields.len();

        // FIXME: make this neat
        if let Some(msgtype) = &message.msgtype {
            self.values.msgtype = Some(*msgtype);
        }
        if required_fields.contains(&"msgtype".to_string()) && !self.values.msgtype.is_none() {
            required_count -= 1;
        }
        if let Some(address) = &message.address {
            self.values.address = Some(*address);
        }
        if required_fields.contains(&"address".to_string()) && !self.values.address.is_none() {
            required_count -= 1;
        }
        if let Some(systemtime) = &message.systemtime {
            self.values.systemtime = Some(systemtime.to_string());
        }
        if required_fields.contains(&"systemtime".to_string()) && !self.values.systemtime.is_none()
        {
            required_count -= 1;
        }
        if let Some(received) = &message.received {
            self.values.received = Some(received.to_string());
        }
        if required_fields.contains(&"received".to_string()) && !self.values.received.is_none() {
            required_count -= 1;
        }
        if let Some(callsign) = &message.callsign {
            self.values.callsign = Some(callsign.to_string());
        }
        if required_fields.contains(&"callsign".to_string()) && !self.values.callsign.is_none() {
            required_count -= 1;
        }
        if let Some(altitude) = &message.altitude {
            self.values.altitude = Some(*altitude);
        }
        if required_fields.contains(&"altitude".to_string()) && !self.values.altitude.is_none() {
            required_count -= 1;
        }
        if let Some(ground_speed) = &message.ground_speed {
            self.values.ground_speed = Some(*ground_speed);
        }
        if required_fields.contains(&"ground_speed".to_string())
            && !self.values.ground_speed.is_none()
        {
            required_count -= 1;
        }
        if let Some(heading) = &message.heading {
            self.values.heading = Some(*heading);
        }
        if required_fields.contains(&"heading".to_string()) && !self.values.heading.is_none() {
            required_count -= 1;
        }
        if let Some(latitude) = &message.latitude {
            self.values.latitude = Some(*latitude);
        }
        if required_fields.contains(&"latitude".to_string()) && !self.values.latitude.is_none() {
            required_count -= 1;
        }
        if let Some(longitude) = &message.longitude {
            self.values.longitude = Some(*longitude);
        }
        if required_fields.contains(&"longitude".to_string()) && !self.values.longitude.is_none() {
            required_count -= 1;
        }
        if let Some(vertical_rate) = &message.vertical_rate {
            self.values.vertical_rate = Some(*vertical_rate);
        }
        if required_fields.contains(&"vertical_rate".to_string())
            && !self.values.vertical_rate.is_none()
        {
            required_count -= 1;
        }
        if let Some(squawk) = &message.squawk {
            self.values.squawk = Some(*squawk);
        }
        if required_fields.contains(&"squawk".to_string()) && !self.values.squawk.is_none() {
            required_count -= 1;
        }
        if let Some(squawk_change) = &message.squawk_change {
            self.values.squawk_change = Some(*squawk_change);
        }
        if required_fields.contains(&"squawk_change".to_string())
            && !self.values.squawk_change.is_none()
        {
            required_count -= 1;
        }
        if let Some(squawk_alert) = &message.squawk_alert {
            self.values.squawk_alert = Some(*squawk_alert);
        }
        if required_fields.contains(&"squawk_alert".to_string())
            && !self.values.squawk_alert.is_none()
        {
            required_count -= 1;
        }
        if let Some(squawk_ident) = &message.squawk_ident {
            self.values.squawk_ident = Some(*squawk_ident);
        }
        if required_fields.contains(&"squawk_ident".to_string())
            && !self.values.squawk_ident.is_none()
        {
            required_count -= 1;
        }
        if let Some(on_ground) = &message.on_ground {
            self.values.on_ground = Some(*on_ground);
        }
        if required_fields.contains(&"on_ground".to_string()) && !self.values.on_ground.is_none() {
            required_count -= 1;
        }

        if required_count == 0 {
            return Some(self);
        }

        None
    }
}

pub struct Flights {
    all: HashMap<u64, Box<Flight>>,
    required_fields: Vec<String>,
}

impl Flights {
    pub fn new(required_fields: Vec<String>) -> Self {
        Flights {
            all: HashMap::new(),
            required_fields: required_fields,
        }
    }

    pub fn update(self: &mut Self, mut message: &mut Message) -> Option<Flight> {
        let uniq = message.uniq().clone();
        match self.all.entry(uniq) {
            Entry::Occupied(_flight) => {
                // FIXME Cut flight at "turnaround" config value
                let mut _flight: &mut Box<Flight> = &mut _flight.into_mut().clone();
                let _last_received = _flight.values.received.clone().unwrap();
                let _last_received = match DateTime::parse_from_rfc3339(&_last_received) {
                    Ok(last) => {
                        let _new_received = message.received.as_ref().unwrap();
                        let _new_received = match DateTime::parse_from_rfc3339(&_new_received) {
                            Ok(new) => {
                                if new - last >= Duration::seconds(900) {
                                    println!("turnaround new {} last {}", new, last);
                                    _flight.reset();
                                    self.all.insert(uniq, _flight.clone());
                                }
                                Some(new)
                            }
                            Err(_) => None,
                        };
                        Some(last)
                    }
                    Err(_) => None,
                };
            }
            Entry::Vacant(_flight) => {
                let mut _flight: Box<Flight> = Box::new(Flight::new());
                self.all.insert(uniq, _flight);
            }
        }
        if let Some(_flight) = self.all.get_mut(&uniq) {
            if let Some(updated) = _flight.update(&mut message, &self.required_fields) {
                return Some(Flight { ..updated.clone() });
            }
        }

        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs::File;
    use std::io::BufRead;
    use std::io::BufReader;
    use std::path::PathBuf;
    use uuid::Uuid;

    fn raw_message(kind: String) -> String {
        // FIXME Enumerate this, make a fixture
        match kind.as_str() {
            "Empty" => "".to_string(),
            "TooFewFields" => ",,,,,,,,,,,,,,,,,,,,,".to_string(),
            "NotAMessage" => "Foo!,,,,,,,,,,,,,,,,,,,,,,".to_string(),
            "AddressMissing" => "MSG,1,111,11111,111111,2020/08/28,21:31:06.256,,,,,,,,,,,,,,,".to_string(),
            "DateMissing" => "MSG,1,111,11111,4677F2,111111,,21:31:06.256,,,,,,,,,,,,,,,".to_string(),
            "TimeMissing" => "MSG,1,111,11111,4677F2,111111,2020/08/28,,,,,,,,,,,,,,,,".to_string(),
            "AllSet" => "MSG,1,111,11111,461E14,111111,2020/08/28,21:35:04.001,2020/08/28,21:35:03.970,FIN7NB,25675,443,23,59.53885,24.02170,-3072,1277,-1,-1,-1,-1".to_string(),
            _ => "".to_string(),
        }
    }

    fn message_series() -> Vec<String> {
        // FIXME make this a fixture
        let mut csv_path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        csv_path.push("tests/messages.csv");
        println!("CSV_PATH {:?}", csv_path);
        let mut msgs: Vec<String> = Vec::new();
        if let Ok(_csv) = File::open(&csv_path) {
            let reader = BufReader::new(_csv);
            for line in reader.lines() {
                msgs.push(line.unwrap());
                //println!("{}", line.unwrap());
            }
        } else {
            panic!("Test message file not found in {:?}", csv_path);
        }

        msgs
    }

    #[test]
    fn message_parsing() {
        // Incomplete messages aren't valid
        for kind in vec![
            "Empty",
            "TooFewFields",
            "NotAMessage",
            "AddressMissing",
            "DateMissing",
            "TimeMissing",
        ] {
            let raw = raw_message(kind.to_string());
            let adsbmessage = Message::parse(&raw);
            assert_eq!(false, adsbmessage.valid());
        }

        // Data is intact after parsing roundtrip
        let raw = raw_message("AllSet".to_string());
        let adsbmessage = Message::parse(&raw);
        assert_eq!(true, adsbmessage.valid());
        assert_eq!(1, adsbmessage.msgtype.unwrap());
        assert_eq!(0x461e14, adsbmessage.address.unwrap());
        assert_eq!(
            "2020-08-28T21:35:04.001Z".to_string(),
            adsbmessage.received.unwrap()
        );
        assert_eq!("FIN7NB".to_string(), adsbmessage.callsign.unwrap());
        assert_eq!(25675, adsbmessage.altitude.unwrap());
        assert_eq!(443, adsbmessage.ground_speed.unwrap());
        assert_eq!(23, adsbmessage.heading.unwrap());
        assert_eq!(59.53885, adsbmessage.latitude.unwrap());
        assert_eq!(24.02170, adsbmessage.longitude.unwrap());
        assert_eq!(-3072, adsbmessage.vertical_rate.unwrap());
        assert_eq!(0o1277, adsbmessage.squawk.unwrap());
        assert_eq!(-1, adsbmessage.squawk_change.unwrap());
        assert_eq!(-1, adsbmessage.squawk_alert.unwrap());
        assert_eq!(-1, adsbmessage.squawk_ident.unwrap());
        assert_eq!(-1, adsbmessage.on_ground.unwrap());
    }

    #[test]
    fn flight_tracking() {
        let msgs = message_series();
        let mut required_fields: Vec<String> = vec![];
        // There has to be a more straightforward way to do this
        for field in vec![
            "received",
            "callsign",
            "altitude",
            "ground_speed",
            "heading",
            "latitude",
            "longitude",
        ] {
            required_fields.push(field.to_string());
        }
        let mut flights = Flights::new(required_fields.to_vec());
        let mut initial_uuid = Uuid::nil();
        let mut initial_callsign = String::new();
        for (index, msg) in msgs.iter().enumerate() {
            let mut adsbmessage = Message::parse(&msg);
            let _flight = match flights.update(&mut adsbmessage) {
                Some(_flight) => {
                    if initial_uuid.is_nil() {
                        initial_uuid = Uuid::from(_flight.uuid);
                    }
                    _flight
                }
                None => {
                    continue;
                }
            };

            println!("initial UUID: {}", initial_uuid);
            println!("({}) Flight:  {}", index, _flight.uuid);

            match index {
                0 => {
                    initial_uuid = Uuid::from(_flight.uuid);
                }
                16 => {
                    // MSG,3,111,11111,461E14,111111,2020/08/28,21:33:56.613,2020/08/28,21:33:56.618,,29350,,,59.39991,23.90265,,,,,,0
                    assert_eq!(initial_uuid, _flight.uuid);
                    initial_callsign = _flight.values.callsign.unwrap();
                    assert_eq!(Some(29350), _flight.values.altitude);
                    assert_eq!(Some(476), _flight.values.ground_speed);
                    assert_eq!(Some(23), _flight.values.heading);
                    assert_eq!(Some(-5504), _flight.values.vertical_rate);
                    assert_eq!(None, _flight.values.squawk);
                }
                26 => {
                    // MSG,5,111,11111,461E14,111111,2020/08/28,21:34:10.505,2020/08/28,21:34:10.494,,28350,,,,,,,0,,0,0
                    assert_eq!(initial_uuid, _flight.uuid);
                    assert_eq!(Some(28350), _flight.values.altitude);
                    assert_eq!(Some(478), _flight.values.ground_speed);
                    assert_eq!(Some(23), _flight.values.heading);
                    assert_eq!(Some(-3520), _flight.values.vertical_rate);
                    assert_eq!(Some(703), _flight.values.squawk);
                }
                44 => {
                    // MSG,4,111,11111,461E14,111111,2020/08/28,21:34:24.405,2020/08/28,21:34:24.385,,,462,23,,,-2368,,,,,0
                    // ^27950
                    assert_eq!(initial_uuid, _flight.uuid);
                    assert_eq!(Some(27950), _flight.values.altitude);
                    assert_eq!(Some(462), _flight.values.ground_speed);
                    assert_eq!(Some(23), _flight.values.heading);
                    assert_eq!(Some(-2368), _flight.values.vertical_rate);
                    assert_eq!(Some(703), _flight.values.squawk);
                }
                47 => {
                    // MSG,8,111,11111,461E14,111111,2020/08/31,06:00:08.267,2020/08/31,06:00:08.276,,,,,,,,,,,,0
                    assert_ne!(initial_uuid, _flight.uuid);
                    assert_ne!(initial_callsign, _flight.values.callsign.unwrap());
                    println!("UUIDs: {} {}", initial_uuid, _flight.uuid);
                }
                _ => {}
            }
        }
    }
}
