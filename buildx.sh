#!/bin/sh -e

apt install -y curl
curl -fsSLO https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKER}.tgz
tar xzvf docker-${DOCKER}.tgz --strip 1 -C /usr/local/bin docker/docker
rm docker-${DOCKER}.tgz

docker info
docker build --platform=local -o . git://github.com/docker/buildx
mkdir -p ~/.docker/cli-plugins && mv buildx ~/.docker/cli-plugins/docker-buildx
docker run --rm --privileged docker/binfmt:a7996909642ee92942dcd6cff44b9b95f08dad64
docker buildx create --name ds1090builder && docker buildx use ds1090builder && docker buildx inspect --bootstrap
sed -i "s/NAMESPACE/${DOCKER_HUB_USERNAME}/g" build.hcl
sed -i "s/VERSION/${VERSION}/g" build.hcl
