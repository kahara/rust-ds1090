#!/bin/bash -l
set -e
if [ "$#" -eq 0 ]; then
  exec ds1090 -vv docker_config.toml
else
  exec "$@"
fi
