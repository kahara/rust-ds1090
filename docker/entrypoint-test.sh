#!/bin/bash -l
set -e
if [ "$#" -eq 0 ]; then
  # TODO: How to get coverage report
  # TODO: How to get junit style XML report
  cargo test
else
  exec "$@"
fi
