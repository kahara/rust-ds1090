group "default" {
    targets = ["ds1090"]
}

target "ds1090" {
    dockerfile = "Dockerfile"
    target = "production"
    platforms = ["linux/amd64", "linux/arm64"]  //, "linux/arm"]  // https://github.com/rust-lang/cargo/issues/7451
    tags = ["NAMESPACE/ds1090:VERSION", "NAMESPACE/ds1090:latest"]
}
